const Discord = require("discord.js");
const { get } = require("snekfetch");  

module.exports.run = async (bot,message,args) => {
        try {
             await get('https://nekos.life/api/v2/img/woof').then(res => {
                const embed = new Discord.RichEmbed()
                .setImage(res.body.url)
                .setColor("0xFFB6C1")
                .setAuthor("Woof! doggo on the way")
                .setFooter("Chino's Café Loves Dogs ♥")
    
                setTimeout(() => {
                    return  message.channel.send({embed});
                }, 100);
            });
        } catch(err) {
            console.log(err);
        }
    }

module.exports.command = {
    name: 'dog',
    aliases: ["woof", "dawg"],
    permission: "",
    description: "sends a pic of a dog.",
    usage: ">dog",
    category: "Images",
    enabled: true
}
